﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour {
	public PlayerMovement playerMovemenetScript;
	public Renderer rnd;
	public float OffsetSpeed;

	// Use this for initialization
	void Start () {
		playerMovemenetScript = GameObject.Find("PlayerWrapper").GetComponent<PlayerMovement>();
		rnd = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		MoveTextureLeft ();
		MoveTextureRight ();
	}

	public void MoveTextureLeft(){
		if (playerMovemenetScript.isMovingLeft && !playerMovemenetScript.isBlocked) {
			rnd.material.mainTextureOffset += new Vector2 (OffsetSpeed * Time.deltaTime, 0);			
		}
	}

	public void MoveTextureRight(){
		if (playerMovemenetScript.isMovingRight  && !playerMovemenetScript.isBlocked) {
			rnd.material.mainTextureOffset += new Vector2 (-OffsetSpeed * Time.deltaTime, 0);			
		}
	}
}
