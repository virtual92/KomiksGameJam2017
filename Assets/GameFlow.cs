﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlow : MonoBehaviour
{
    public static String startingLocationObjName = "CheckPoint1";
    
    private Global global;
    private GameObject deadPanel;
    private AudioSource endGameSound;
    private bool waitingForStop;

    public GameObject DeadPanel
    {
        get { return deadPanel; }
        set { deadPanel = value; }
    }

    private void Awake()
    {
        
        global = GetComponent<Global>();
    }

    // Use this for initialization
    void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.name.Equals("GameOverSound"))
            {
                endGameSound = child.GetComponent<AudioSource>();
            }
        }

        var find = GameObject.Find(startingLocationObjName);
		Debug.Log (find.name);
        global.Player.transform.position = find.transform.position;
        GameObject.Find("BorderOfLife").transform.position = find.transform.position + Vector3.left * 20;
    }

    // Update is called once per frame
    void Update()
    {
        if ((!waitingForStop) && global.PlayerDead && Input.GetButtonDown("Fire1"))
        {
            waitingForStop = true;
            endGameSound.Play();
        }
        else if (waitingForStop && ((!endGameSound.isPlaying) || Input.GetButtonDown("Fire1")))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }


    public void PlayerDed()
    {
        
        foreach (Transform child in deadPanel.transform)
        {
            child.gameObject.SetActive(true);
        }
        foreach (Transform child in gameObject.transform)
        {
            if (child.gameObject.name.Equals("GameOverSound"))
            {
                var audioSource = child.GetComponent<AudioSource>();
                audioSource.enabled = true;
            }
        }

        global.PlayerDead = true;
    }
}