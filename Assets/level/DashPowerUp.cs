﻿using System.Reflection.Emit;
using UnityEngine;

namespace level
{
    public class DashPowerUp : MonoBehaviour
    {
        private GameObject powerUpGuide;
        private BorderOfLife.BorderOfLife borderOfLife;
        private bool activated;

        // Use this for initialization
        void Start()
        {
            powerUpGuide = GameObject.Find("PowerUpGuide");
            borderOfLife = GameObject.Find("Global").GetComponent<Global>().BorderOfLife;
        }

        // Update is called once per frame
        void Update()
        {
            if (activated && (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.LeftShift)))
            {
                foreach (Transform child in powerUpGuide.transform)
                {
                    child.gameObject.SetActive(false);
                }
                Time.timeScale = 1;
                GameObject.Destroy(gameObject);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            GameFlow.startingLocationObjName = "CheckPoint2";
            Debug.Log("Coll");
            borderOfLife.Speed = borderOfLife.Speed * 2;
            this.GetComponent<BoxCollider2D>().enabled = false;
            Time.timeScale = 0;
            GameObject.Find("Global")
                .GetComponent<Global>().PlayerWrapper
                .GetComponent<PlayerMovement>().canDash = true;
            foreach (Transform child in powerUpGuide.transform)
            {
                child.gameObject.SetActive(true);
            }
            activated = true;
        }
    }
}