﻿using UnityEngine;
using wave;

namespace Player
{
	public class WaveShotter : MonoBehaviour
	{
		private WaveManager waveManager;
		
		private void Awake()
		{
			waveManager = GameObject.Find("Global").GetComponent<WaveManager>();
		}

		// Update is called once per frame
		void Update () {

			if (Input.GetButtonDown("Fire1"))
			{
				waveManager.FireWave(this.transform.position);
				//Debug.Log("Firing wave");
			}
			
			
		}
	}
}
