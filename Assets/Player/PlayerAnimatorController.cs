﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorController : MonoBehaviour {
	Animator emitterAnimator;
	public Animator anim;
	public bool isAnimating;
	public bool isJumping;
	public bool isRunning;

	// Use this for initialization
	void Start () {
		isRunning = false;
		anim = GameObject.Find("PlayerSprite").GetComponent<Animator>();	
		emitterAnimator = transform.GetChild (0).GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void StopAnimation(){
		anim.Play ("player_idle");
	}
	public void AnimatorRunLeft(){
		if (!isRunning) {
			isRunning = true;
			anim.Play ("running");
		}
	}
	public void AnimatorRunRight(){
		if (!isRunning) {
			isRunning = true;
			anim.Play ("running");
		}
	}
	public void AnimatorJump(){
		isJumping = true;
		anim.Play("jump");
	}

	public void AnimatorFalling(){
		anim.Play("falling");
	}


	public void Climbing(){
		anim.Play ("climb_up");
	}

	public void StoppedRunning(){
		isRunning = false;
		anim.Play ("run_end");
	}

	public bool ifJumping(){
		return false;
	}

	public void AnimatorEmittWaves(){
		//emitterAnimator.SetBool("isWaving", true);
	}
}
