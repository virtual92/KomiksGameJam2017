﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCollider : MonoBehaviour {

	private GameObject Player;

	// Use this for initialization
	void Start () {
		Player = GameObject.Find("Global").GetComponent<Global>().PlayerWrapper;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionStay2D(Collision2D collision) {
		if (collision.gameObject.tag == "FLOOR") {
			Player.GetComponent<PlayerMovement>().isJumping = false;
			Player.GetComponent<PlayerMovement>().canJump = true;
		}
	}
	void OnCollisionExit2D(Collision2D collision) {
		if (collision.gameObject.tag == "FLOOR") {
			Player.GetComponent<PlayerMovement>().canJump = false;
		}
	}
}
