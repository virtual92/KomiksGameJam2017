﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallenOut : MonoBehaviour {
	private Global global;

	private void Awake()
	{
		global = GameObject.Find("Global").GetComponent<Global>();
	}

	public void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "DEADFLOOR") {
			global.GetComponent<GameFlow>().PlayerDed();
		}
	}
}
