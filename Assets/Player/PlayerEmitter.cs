﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEmitter : MonoBehaviour {
	public PlayerAnimatorController anim;
	public GameObject Emitter;
	public float Wavelength;

	// Use this for initialization
	void Start () {
		anim = GetComponent<PlayerAnimatorController> ();
		Emitter = transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		Emitt ();
	}

	public void Emitt(){
		if (Input.GetKeyDown(KeyCode.Space)) {
			anim.AnimatorEmittWaves ();
		}
	}

	public void ChangeWavelengthSize(float size){
		Emitter.transform.localScale = new Vector3 (size, size, 0);
	}
}
