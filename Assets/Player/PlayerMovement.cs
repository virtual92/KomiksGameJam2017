﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMovement : MonoBehaviour {
	public GameObject PlayerSprite;
	public GameObject Player;
	public Rigidbody2D rg;
	public GameObject SpriteDashTrailer;
	public PlayerAnimatorController anim;
	public float JumpForce = 5000f;
	public float MovementSpeedLimit = 500f;
	public float MovementSpeed;
	public float ClimbSpeed = 5f;
	public float initialGravityScale;
	public bool isMovingRight, isMovingLeft, canDash, canJump, isJumping, canClimb, isClimbing, isDashing;
	public bool isBlocked;
	public float maximumXVelocity = 4;
	Vector3 lastPosition;

	private AudioSource jumpSound;
	private AudioSource runSound;
	private AudioSource dashSound;

	private float dashCooldown;
	public float DashCooldown = 1f;

	// Use this for initialization
	void Start() {
		
		isBlocked = false;
		isMovingRight = isMovingLeft = false;
		PlayerSprite = GameObject.Find("PlayerSprite");
		Player = GameObject.Find("Player");
		SpriteDashTrailer = GameObject.Find ("SpriteDashTrailer");
		anim = GetComponent<PlayerAnimatorController>();
		Debug.Log(anim);
		rg = Player.GetComponent<Rigidbody2D>();
		initialGravityScale = rg.gravityScale;
		jumpSound = GameObject.Find("JumpSound").GetComponent<AudioSource>();
		runSound = GameObject.Find("RunSound").GetComponent<AudioSource>();
		dashSound = GameObject.Find("DashSound").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void FixedUpdate() {
		isBlocked = Math.Round(lastPosition.x, 3) == Math.Round(PlayerSprite.transform.position.x, 3);
		lastPosition = PlayerSprite.transform.position;
		Jump();
		MoveLeft();
		MoveRight();
		LandedOrFalling ();
		Climb();
		if (canDash)
		{
			TrailingSpeed();
		}

//		if ((isMovingLeft || isMovingRight) && !runSound.enabled)
//		{
//			runSound.enabled = true;
//			runSound.Play();
//		}
//		if (!isMovingLeft && !isMovingRight && runSound.enabled)
//		{
//			runSound.enabled = false;
//		}

		if (rg.velocity.x > maximumXVelocity)
		{
			rg.velocity = new Vector2(maximumXVelocity, rg.velocity.y);
		}
	}

	void TrailingSpeed(){
		if (!isDashing && dashCooldown > 0f) {
			dashCooldown -= Time.deltaTime;
		} else if (Input.GetKeyDown(KeyCode.LeftShift) && (isMovingLeft || isMovingRight) && !isDashing){
			dashCooldown = DashCooldown;
			SpriteDashTrailer.GetComponent<TrailGenerator> ().StartTrailing (true);
			StartDash();
		}
	}

	public void StartDash() {
		if (!isDashing) {
			//rg.AddForce(new Vector2(MovementSpeed * 20f, 0));
			rg.velocity = new Vector2(MovementSpeed, 0);

			rg.AddForce(new Vector2(MovementSpeed * 40f, 0));
			rg.gravityScale = 0;
			MovementSpeed *= 2;
			MovementSpeedLimit *= 2;
			isDashing = true;
			dashSound.Play ();
		}
	}

	public void StopDash() {
		if (isDashing) {
			rg.gravityScale = initialGravityScale;
			MovementSpeed /= 2;
			MovementSpeedLimit /= 2;
			isDashing = false;
			dashSound.Stop();
		}
	}

	void Jump() {
		if (Input.GetKey(KeyCode.Space) && canJump == true && isJumping == false) {
			isJumping = true;
			runSound.Stop ();
			jumpSound.Play();
			anim.AnimatorJump ();
			rg.AddForce(new Vector2(0f, JumpForce));
		}
	}

	void LandedOrFalling() {
		if (canJump) {
			if (isMovingLeft || isMovingRight) {
				if (!isJumping) anim.anim.Play ("running");
			} else {
				anim.StopAnimation();
			}
		} else if (!canClimb && rg.velocity.y < 0) {
			anim.AnimatorFalling();
		}
	}

	void Climb() {
		if (canClimb == true) {
			if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow)) {
				anim.anim.enabled = true;
				anim.Climbing();
				isClimbing = true;
				rg.gravityScale = 0;
				if (Input.GetKey(KeyCode.UpArrow)) {
					rg.velocity = new Vector2(rg.velocity.x, ClimbSpeed);
				} else if (Input.GetKey(KeyCode.DownArrow)) {
					rg.velocity = new Vector2(rg.velocity.x, -ClimbSpeed);
				}
			} else if (isClimbing) {
				rg.velocity = new Vector2(rg.velocity.x, 0f);
				anim.anim.enabled = false;
			}
		} else {
			isClimbing = false;
			anim.anim.enabled = true;
			// todo: if climb animation persists, perhaps uncomment this:
			// anim.AnimatorFalling();
			// Debug.Log("stop animation");
			if (!isDashing) rg.gravityScale = initialGravityScale;
		}
	}

	void MoveLeft() {
		if (Input.GetKey (KeyCode.LeftArrow) && rg.velocity.x > -MovementSpeedLimit) {
			PlayerSprite.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
			if (!isClimbing) anim.AnimatorRunLeft();
			isMovingLeft = true;
			rg.AddForce(new Vector2(-MovementSpeed, 0));
			if (!runSound.enabled) {
				runSound.enabled = true;
				runSound.Play ();
			}
		}
		StopMovement(KeyCode.LeftArrow);
	}

	void MoveRight() {
		if (Input.GetKey(KeyCode.RightArrow) && rg.velocity.x < MovementSpeedLimit) {
			PlayerSprite.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
			if (!isClimbing) anim.AnimatorRunRight();
			isMovingRight = true;
			rg.AddForce(new Vector2(MovementSpeed, 0));
			if (!runSound.enabled) {
				runSound.enabled = true;
				runSound.Play ();
			}
		}
		StopMovement(KeyCode.RightArrow);
	}

	void StopMovement(KeyCode keyUp) {
		if (!isDashing && Input.GetKeyUp(keyUp)) {
			anim.StoppedRunning();
			isMovingLeft = isMovingRight = false;
			rg.velocity = new Vector3 (0, rg.velocity.y, 0);
			runSound.enabled = false;
		}
	}

	void StopMovement() {
		anim.StoppedRunning();
		isBlocked = true;
		isMovingLeft = isMovingRight = false;
		rg.velocity = new Vector3 (0, rg.velocity.y, 0);
		runSound.enabled = false;
	}
}
