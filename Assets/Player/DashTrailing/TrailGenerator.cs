﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailGenerator : MonoBehaviour {
	public GameObject PlayerSprite;
	public GameObject PlayerWrapper;
	public PlayerMovement PlayerMovement;
	public GameObject GhostPrefab;
	public int GhostCounts;
	public int currentGhostCounts;
	public float TimeToInstantiateNext;
	private float currentTime;
	private bool isTrailing;

	// Use this for initialization
	void Start () {
		isTrailing = false;
		PlayerSprite = GameObject.Find ("PlayerSprite");
		PlayerWrapper = GameObject.Find ("PlayerWrapper");
		PlayerMovement = PlayerWrapper.GetComponent<PlayerMovement>();
		SetUpTrailParameters ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isTrailing) {
			if (currentGhostCounts > 0) {
				if (currentTime < 0) {
					InstantiateGhost ();
					currentTime = TimeToInstantiateNext;
					currentGhostCounts--;
				} else {
					currentTime -= Time.deltaTime;
				}
			} else {
				if (isTrailing) {
					PlayerMovement.StopDash();
				}
				isTrailing = false;
				SetUpTrailParameters ();
			}
		}

	}

	public void InstantiateGhost(){
		Instantiate (GhostPrefab, PlayerSprite.transform.position, PlayerSprite.transform.rotation);
	}

	public void StartTrailing(bool isTrailing){
		if (this.isTrailing == false) {
			this.isTrailing = isTrailing;
		}
	}

	public void SetUpTrailParameters(){
		currentTime = TimeToInstantiateNext;
		currentGhostCounts = GhostCounts;
	}
}
