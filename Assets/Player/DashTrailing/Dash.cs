﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {
	public float TimeToFadeOut;
	private Color initialColor;
	SpriteRenderer ghostSprite;
	private Color currentColor;
	public float timeLeft;

	// Use this for initialization
	void Start () {
		ghostSprite = GetComponent<SpriteRenderer> ();	
		initialColor = ghostSprite.color;
	}
	
	// Update is called once per frame
	void Update () {
		currentColor = Color.Lerp(new Color(1,1,1,1), new Color(1,1,1,0), Time.deltaTime / TimeToFadeOut);
		ghostSprite.color = currentColor;
		TimeToFadeOut -= Time.deltaTime;
	}

	public void DestroyItself(){
		Destroy (gameObject);
	}
}
