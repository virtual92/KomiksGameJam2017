﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public float Speed;
	public GameObject Player;
	public CameraDetector HorizontalDetector;
	public CameraDetector VerticalDetector;
	private Vector3 initialPosition;
	private PlayerMovement playerMovementScript;

	// Use this for initialization
	void Start () {
		playerMovementScript = GameObject.Find ("PlayerWrapper").GetComponent<PlayerMovement>();
		initialPosition = transform.position;
		Player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += new Vector3 (Speed * Time.deltaTime, 0, 0);
		if(Camera.main.WorldToScreenPoint(Player.transform.position).x > Screen.width/2){
			Camera.main.transform.position = new Vector3(Player.transform.position.x,	initialPosition.y, initialPosition.z);
		}
	}
}
