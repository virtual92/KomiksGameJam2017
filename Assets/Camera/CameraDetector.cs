﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDetector : MonoBehaviour {
	public bool isPlayerFallenOut;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "DeadFloor") {
			isPlayerFallenOut = true;
		}
	}
}
