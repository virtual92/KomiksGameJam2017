﻿using Kino;
using UnityEngine;

namespace BorderOfLife
{
    public class BorderOfLife : MonoBehaviour
    {
        private Global global;
        private GameObject camera;

        public float GlitchDistanceStart = 4f;
        public float GlitchMaxValue = 0.8f;

        public float Speed = 2f;
        private float currentSpeed;
        public float LengthToCatchUp = 5f;

        private DigitalGlitch digitalGlitch;
        

        private void Awake()
        {
            global = GameObject.Find("Global").GetComponent<Global>();
            
        }


        // Use this for initialization
        void Start()
        {
            currentSpeed = Speed;
            digitalGlitch = global.Camera.GetComponent<DigitalGlitch>();
        }

        // Update is called once per frame
        void Update()
        {
            var dist = Mathf.Abs(transform.position.x - global.Player.transform.position.x);
            if (dist > LengthToCatchUp)
            {
                float overDistance = dist - LengthToCatchUp;
                float faster = Mathf.Clamp(overDistance, 0, 10f);
                currentSpeed = Speed + faster;
            }
            else
            {
                currentSpeed = Speed;
            }
            
            this.transform.Translate(Vector3.right * Time.deltaTime * currentSpeed);

            var distance = Mathf.Abs(global.Player.transform.position.x - this.transform.transform.position.x);
            if (distance < GlitchDistanceStart)
            {
                var instensity = Mathf.Lerp(GlitchMaxValue, 0, Mathf.Clamp(distance / GlitchDistanceStart, 0, 1));
                digitalGlitch.intensity = instensity;
            }
            else
            {
                digitalGlitch.intensity = 0;
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
			if(other.gameObject.CompareTag("Player"))
            	global.GetComponent<GameFlow>().PlayerDed();
        }
    }
}