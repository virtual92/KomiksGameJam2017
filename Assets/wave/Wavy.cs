﻿using UnityEngine;

namespace wave
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Wavy : MonoBehaviour
    {
        public bool Faulty = false;

        private WaveManager waveManager;
        private SpriteRenderer spriteRenderer;
        private Global global;


        private void Awake()
        {
            var find = GameObject.Find("Global");
            waveManager = find.GetComponent<WaveManager>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            global = find.GetComponent<Global>();
            waveManager.AddWavy(this);
        }


        private void OnDestroy()
        {
            waveManager.RemoveWavy(this);
        }

        public SpriteRenderer GetSpriteRenderer()
        {
            return spriteRenderer;
        }

        // Use this for initialization
        void Start()
        {
        }



        // Update is called once per frame
        void Update()
        {
            var pos = global.Player.transform.position;
            spriteRenderer.material.SetVector("_playerPos", new Vector4(pos.x, pos.y, pos.z, 0));
        }
    }
}