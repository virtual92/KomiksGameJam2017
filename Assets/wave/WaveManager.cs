﻿using System.Collections.Generic;
using UnityEngine;

namespace wave
{
    public class WaveManager : MonoBehaviour
    {
        public float WaveDuration = 4f;
        public float WaveSpeed = 4.0f;
        public float WaveMaxLength = 10f;

        public float WaveSize = 1.2f;
        public float WaveBetweenSize = 0.13f;
        public int WavesAmount = 7;
        public float PlayerVisionLength = 2;

        private List<Wavy> wavies = new List<Wavy>();
        private Vector4 epicentrum = Vector4.zero;
        private bool active = false;
        private float remainingTime = 0;
        private float waveR = 0;

        private Global global;


        private void Awake()
        {
            global = GetComponent<Global>();
        }

        void Start()
        {
        }

        // Update is called once per frame

        void Update()
        {
            if (active)
            {
                remainingTime -= Time.deltaTime;
                if (remainingTime <= 0)
                {
                    WaveStop();
                }
                else
                {
                    waveR = waveR + Time.deltaTime * WaveSpeed;
                    foreach (var wavy in wavies)
                    {
                        var material = wavy.GetSpriteRenderer().material;
                        material.SetFloat("_rEnd", waveR);
                    }
                }
            }
        }

        private void WaveStop()
        {
//            Debug.Log("WaveStop");
            foreach (var wavy in wavies)
            {
                wavy.GetSpriteRenderer().material.SetFloat("_rEnd", 0);
            }
            active = false;
        }

        public void FireWave(Vector3 epicentrum)
        {
            if (!active && !global.PlayerDead)
            {
               // Debug.Log("Firing wave");
                this.epicentrum = new Vector4(epicentrum.x, epicentrum.y, epicentrum.z);
                foreach (var wavy in wavies)
                {
                    wavy.GetSpriteRenderer().material.SetVector("_centrum", this.epicentrum);
                }
                active = true;
                remainingTime = WaveDuration;
                waveR = 0;

                foreach (Transform child in this.transform)
                {
                    if (child.name.Equals("WaveSound"))
                    {
                        var audioSource = child.GetComponent<AudioSource>();
                        audioSource.Play();
                    }
                }
            }
        }

        public void AddWavy(Wavy wavy)
        {
            wavies.Add(wavy);
            SetupWave(wavy);
        }

        public void RemoveWavy(Wavy wavy)
        {
            wavies.Remove(wavy);
        }

        private void SetupWave(Wavy wavy)
        {
            var material = wavy.GetSpriteRenderer().material;
            material.SetFloat("_rSize", WaveSize);
            material.SetFloat("_betweenSize", WaveBetweenSize);
            material.SetInt("_amount", WavesAmount);
            material.SetFloat("_rEnd", 0);
            material.SetFloat("_rMax", WaveMaxLength);
            material.SetFloat("_rPlayerVision", PlayerVisionLength);
        }
    }
}