﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

 Shader "SuperWave"
 {  
     Properties
     {
        _MainTex ("Sprite Texture", 2D) = "white" {}
        _rEnd ("REnd",  Float) = 1.0
        _rSize ("RSize",  Float) = 1.0
        _rMax ("rMax", Float) = 3000.0
        _betweenSize ("BetweenSize",  Float) = 1.0
        _amount ("Amount", Int) = 4
        _centrum ("Centrum", Vector) = (0,0,0,0)
        _customColorOverride ("CustomColorOverride", Int) = 0
        _customColor ("CustomColor", COLOR) = (0, 0, 0, 0)
        _playerPos ("PlayerPosition", Vector) = (0, 0, 0, 0)
        _rPlayerVision ("Player Vision Range", Float) = 1.0
        _glowThreshold ("glow threshold", Float) = 0.12
        _isFaulty ("if Faulty", Int) = 0
     }
     SubShader
     {
         Tags 
         { 
             "RenderType" = "Opaque" 
             "Queue" = "Transparent+1" 
         }
 
         Pass
         {
             ZWrite Off
             Blend SrcAlpha OneMinusSrcAlpha 
  
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
  
             sampler2D _MainTex;
             float _rEnd;
             float _rSize;
             float _rMax;
             float _betweenSize;
             int _amount;
             float4 _centrum;
             int _customColorOverride;
             float4 _customColor;
             float4 _playerPos;
             float _rPlayerVision;
             float _glowThreshold;
             int _isFaulty;
             struct Vertex
             {
                 float4 vertex : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
                 float2 uv2 : TEXCOORD1;
             };
     
             struct Fragment
             {
                 float4 vertex : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
                 float3 worldPos : TEXCOORD1;
             };
  
             Fragment vert(Vertex v)
             {
                 Fragment o;
     
                 o.vertex = UnityObjectToClipPos(v.vertex);
                 o.uv_MainTex = v.uv_MainTex;
                 //o.uv2 = v.uv2;
                 
                 float3 worldPos = mul (unity_ObjectToWorld, v.vertex).xyz;
                 
                 o.worldPos = worldPos;
                 
     
                 return o;
             }
                                                     
             float4 frag(Fragment IN) : COLOR
             {
                 half4 c;
                 if(_customColorOverride == 0){
                 c = tex2D (_MainTex, IN.uv_MainTex);
                 } else {
                 c = _customColor;
                 }               
                 
                 float distancee = distance(_centrum.xy, IN.worldPos.xy);
                 
                 float oldAlpha = c.a;
                 c.a = 0.0;
                 float rEndEhanced = _rEnd - _rSize;
               //  if(distancee > _rStart){
                    for(int i = 0; i < _amount; i++){
                        float currentRMin = rEndEhanced - i * (_rSize + _betweenSize);
                        float currentRMax = rEndEhanced - i * (_rSize + _betweenSize) + _rSize;
                        
                        if(distancee > currentRMin && distancee < currentRMax && distancee < _rMax){
                        
                            float current = distancee - currentRMin;
                            float normalized = current / _rSize;
                            
                            c.a = normalized * oldAlpha;
                        }
                    }
               //  }
               
               float playerDistance = distance(_playerPos.xy, IN.worldPos.xy);
               if(playerDistance < _rPlayerVision && _customColorOverride == 0){
                    float normalized = 1 - (playerDistance / _rPlayerVision);
                    if(c.r > _glowThreshold && playerDistance < _rPlayerVision / 1.5){
                           float normalizedG = 1 - (playerDistance / (_rPlayerVision / 1.5));
                           float normalizedGLerp = (1.0 - normalizedG) + 1 * (normalizedG);
                           if(_isFaulty == 0){
                           c.g = c.g * (1.0 - normalizedG) + 1 * (normalizedG);
                           //c.g = 1.0;
                           } else {
                           c.r = c.r * (1.0 - normalizedG) + 1 * (normalizedG);
                           }
                           //c.g = 
                           //
                           
                           //c.g = 1 * normalizedG;
                           
                    }             
                    
                    if(c.a < normalized){
                        c.a = normalized;
                    }
                    
               }
               
                 if(_isFaulty != 0){
                    c.r = c.r + 0.3;
                    //c.g = c.g + 0.6;
                 }
 

                 return c;
             }
 
             ENDCG
         }
     }
 }