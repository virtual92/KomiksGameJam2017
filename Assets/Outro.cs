﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outro : MonoBehaviour {

	public GameObject first, isIt, fatalError, fatalError2, thatsAll, theEnd;
	private float time;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
	}

	void OnGUI() {
		if (time > 7f && time < 7.1f) {
			isIt.GetComponent<Canvas>().enabled = true;
        	// GUI.Label(new Rect(340, 420, 100, 20), "...or is it?", style);
		} else if (time > 12f && time < 12.1f) {
			first.GetComponent<Canvas>().enabled = false;
			isIt.GetComponent<Canvas>().enabled = false;
			fatalError.GetComponent<Canvas>().enabled = true;
		} else if (time > 12.1f && time < 18f) {
			if (time * 100 % 100 > 50) {
				fatalError2.GetComponent<Canvas>().enabled = true;
			} else {
				fatalError2.GetComponent<Canvas>().enabled = false;
			}
		} else if (time > 18f && time < 18.1f) {
			fatalError2.GetComponent<Canvas>().enabled = false;
			fatalError.GetComponent<Canvas>().enabled = false;
			theEnd.GetComponent<Canvas>().enabled = true;
		} else if (time > 21f && time < 21.1f) {
			thatsAll.GetComponent<Canvas>().enabled = true;
		}
    }
}
