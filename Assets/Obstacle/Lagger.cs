﻿using UnityEngine;

namespace Obstacle
{
    public class Lagger : MonoBehaviour
    {

        private bool lagged;
        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if ((!lagged) && other.gameObject.name.Equals("Player"))
            {
                lagged = true;
                GameObject.Find("Global").GetComponent<Global>().PlayerWrapper.SendMessage("Lag");
            }
        }
    
    
    }
}