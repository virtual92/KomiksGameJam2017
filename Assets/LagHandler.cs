﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LagHandler : MonoBehaviour {
	public float LagTime;
	private float currentTime;
	private float movementSpeed;
	public Color VisibleColor;
	public Color HiddenColor;
	GameObject LagObject;
	GameObject Player;
	public bool startLagging;

	private Animator playerAnimator;

	// Use this for initialization
	void Start () {
		startLagging = false;
		LagObject = GameObject.Find ("Lag");
		Player = GameObject.Find ("PlayerWrapper");
		playerAnimator = GameObject.Find("PlayerSprite").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (startLagging) {
			if (currentTime > 0) {
				currentTime -= Time.deltaTime;
				Player.GetComponent<PlayerMovement> ().MovementSpeed = 0;
			} else
				DoneLag ();
		}
	}


	void Lag()
	{
		startLagging = true;
		currentTime = LagTime;
		LagObject.GetComponent<SpriteRenderer> ().color = VisibleColor;
		var playerMovement = Player.GetComponent<PlayerMovement>();
		playerMovement.enabled = false;
		movementSpeed = playerMovement.MovementSpeed;
		playerAnimator.enabled = false;
	}

	void DoneLag(){
		startLagging = false;
		var playerMovement = Player.GetComponent<PlayerMovement>();
		playerMovement.enabled = true;
		Player.GetComponent<PlayerMovement> ().MovementSpeed = movementSpeed;
		Player.GetComponent<PlayerMovement>().anim.isRunning = false;
		LagObject.GetComponent<SpriteRenderer> ().color = HiddenColor;
		playerAnimator.enabled = true;
	}
	
}
