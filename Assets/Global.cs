﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
	private GameObject player;
	private GameObject camera;
	private GameObject playerWrapper;
	private BorderOfLife.BorderOfLife borderOfLife;
	private bool playerDead;

	public BorderOfLife.BorderOfLife BorderOfLife
	{
		get { return borderOfLife; }
		set { borderOfLife = value; }
	}

	public GameObject PlayerWrapper
	{
		get { return playerWrapper; }
		set { playerWrapper = value; }
	}

	public GameObject Camera
	{
		get { return camera; }
		set { camera = value; }
	}

	public bool PlayerDead
	{
		get { return playerDead; }
		set { playerDead = value; }
	}

	public GameObject Player
	{
		get { return player; }
		set { player = value; }
	}

	private void Awake()
	{
		player = GameObject.Find("Player");
		playerWrapper = GameObject.Find("PlayerWrapper");
		camera = GameObject.Find("Main Camera");
		borderOfLife = GameObject.Find("BorderOfLife").GetComponent<BorderOfLife.BorderOfLife>();
	}

	// Use this for initialization

	void Start () {
		
	}

	// Update is called once per frame

	void Update () {
		
	}
}
