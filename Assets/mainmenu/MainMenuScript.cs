﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace mainmenu
{
    public class MainMenuScript : MonoBehaviour
    {
        public Button startButton;
        public Button authorsButton;
		public Animator anim;

        // Use this for initialization
        void Start()
        {
			anim = GetComponent<Animator> ();

	        startButton.onClick.AddListener(() =>
            {
					StartIntro();
            });
            
            authorsButton.onClick.AddListener(() =>
            {
					ShowAuthors();
            });
            
            
        }

		public void StartIntro(){
			anim.Play("intro");	
		}

		public void ChangeToIntroScene(){
			SceneManager.LoadScene ("mainmenu/intro");
		}

		public void ShowAuthors(){
			SceneManager.LoadScene("authors");
		}

    }
}