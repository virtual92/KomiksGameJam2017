﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelfWritter : MonoBehaviour {
	public string text;
	string empty;
	string letter;
	public float TimeToNextLetter;
	public float currentTime;
	private int currentPosition;
	private UnityEngine.UI.Text TextField;
	bool passedDescription;
	public string SceneName;
	public string alertTextBelow;
	public GameObject PressSpacebar;
	public AudioSource typingSound;

	// Use this for initialization
	void Start () {
		PressSpacebar = GameObject.Find ("PressSpacebar");
		typingSound = GameObject.Find("typing").GetComponent<AudioSource>();
		currentPosition = 0;
		Debug.Log (text.Length);
		TextField = GetComponent<UnityEngine.UI.Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (passedDescription && Input.GetKeyDown (KeyCode.Space)) {
			SceneManager.LoadScene (SceneName);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			TextField.text = text;
			PressSpacebar.GetComponent<UnityEngine.UI.Text> ().text = alertTextBelow;
			passedDescription = true;
		}else {
			if (!passedDescription) {
				if (currentTime > 0) {
					currentTime -= Time.deltaTime;
				} else {
					if (currentPosition < text.Length) {
						letter = text [currentPosition].ToString ();
						currentPosition++;
						empty = string.Concat (empty, letter);
						TextField.text = empty;
						currentTime = TimeToNextLetter;
					} else {
						passedDescription = true;
					}
				}
			}
		}
	}
}
