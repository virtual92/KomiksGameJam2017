﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLadders : MonoBehaviour {
	public PlayerMovement playerMovement;
	public SpriteRenderer playerRenderer;
	public GameObject[] ladders;

	// Use this for initialization
	void Start () {
		playerMovement = GameObject.Find("PlayerWrapper").GetComponent<PlayerMovement>();
		playerRenderer = GameObject.Find("PlayerSprite").GetComponent<SpriteRenderer>();
		ladders = GameObject.FindGameObjectsWithTag("LADDER");
	}
	
	// Update is called once per frame
	void Update () {
		var canClimb = false;
		foreach (var ladder in ladders) {
			// todo: perhaps renderer.bounds should be static (get in Start and assume it don't change)
			var playerBounds = playerRenderer.bounds;
			var ladderBounds = ladder.GetComponent<SpriteRenderer>().bounds;
			if (ladderBounds.Intersects(playerBounds)) {
				canClimb = true;
				break;
			}
		}
		playerMovement.canClimb = canClimb;
	}
}
